package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveWPA extends LjlObject {
    public String algorithmType;
    public String sharedKey;
    public int wpaKeyLength;
}
