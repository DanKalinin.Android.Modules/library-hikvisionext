package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveVideo extends LjlObject {
    public String videoCodecType;
    public int videoResolutionWidth;
    public int videoResolutionHeight;
    public String videoQualityControlType;
    public int maxFrameRate;
}
