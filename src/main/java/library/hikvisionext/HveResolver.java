package library.hikvisionext;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Looper;
import library.java.lang.LjlObject;

public class HveResolver extends LjlObject implements NsdManager.ResolveListener {
    HveBrowser browser;

    @Override
    public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {

    }

    @Override
    public void onServiceResolved(NsdServiceInfo serviceInfo) {
        byte[] path = serviceInfo.getAttributes().get("path");

        HveEndpoint endpoint = new HveEndpoint();
        endpoint.name = serviceInfo.getServiceName();
        endpoint.host = serviceInfo.getHost().getHostName();
        endpoint.ip = serviceInfo.getHost().getHostAddress();
        endpoint.port = serviceInfo.getPort();
        endpoint.path = (path == null) ? "/" : new String(path);

        new Handler(Looper.getMainLooper()).post(() -> browser.listeners.endpointFound(browser, endpoint));
    }

    public static HveResolver resolver(HveBrowser browser) {
        HveResolver ret = new HveResolver();
        ret.browser = browser;
        return ret;
    }
}
