package library.hikvisionext;

import android.os.Handler;
import android.os.Looper;
import java.io.Closeable;
import library.glibext.GeException;
import library.java.lang.LjlObject;

public class HveClient extends LjlObject implements Closeable {
    private long object;
    public String base;
    public String username;
    public String password;

    @Override
    protected void finalize() throws Throwable {
        close();
    }

    @Override
    public native void close();

    public static native HveClient client(String base, String username, String password);

    public static HveClient client(HveEndpoint endpoint, String username, String password) {
        String base = "http://" + endpoint.ip + ":" + endpoint.port + endpoint.path;
        return HveClient.client(base, username, password);
    }

    public native void abort();

    // ActivateStatusGet

    public native HveActivateStatus activateStatusGetSync() throws GeException;

    public interface ActivateStatusGetAsyncCallback {
        void callback(HveActivateStatus activateStatus, GeException exception);
    }

    public void activateStatusGetAsync(ActivateStatusGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveActivateStatus activateStatus = activateStatusGetSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(activateStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // ChallengeGet

    public native HveChallenge challengeGetSync(HvePublicKey publicKey) throws GeException;

    public interface ChallengeGetAsyncCallback {
        void callback(HveChallenge challenge, GeException exception);
    }

    public void challengeGetAsync(HvePublicKey publicKey, ChallengeGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveChallenge challenge = challengeGetSync(publicKey);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(challenge, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Activate

    public native HveResponseStatus activateSync(HveActivateInfo activateInfo) throws GeException;

    public interface ActivateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void activateAsync(HveActivateInfo activateInfo, ActivateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = activateSync(activateInfo);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // Reboot

    public native HveResponseStatus rebootSync() throws GeException;

    public interface RebootAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void rebootAsync(RebootAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = rebootSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // DeviceInfoGet

    public native HveDeviceInfo deviceInfoGetSync() throws GeException;

    public interface DeviceInfoGetAsyncCallback {
        void callback(HveDeviceInfo deviceInfo, GeException exception);
    }

    public void deviceInfoGetAsync(DeviceInfoGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveDeviceInfo deviceInfo = deviceInfoGetSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(deviceInfo, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // TimeUpdate

    public native HveResponseStatus timeUpdateSync(HveTime time) throws GeException;

    public interface TimeUpdateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void timeUpdateAsync(HveTime time, TimeUpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = timeUpdateSync(time);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // NetworkInterfacesGet

    public native HveNetworkInterface[] networkInterfacesGetSync() throws GeException;

    public interface NetworkInterfacesGetAsyncCallback {
        void callback(HveNetworkInterface[] networkInterfaces, GeException exception);
    }

    public void networkInterfacesGetAsync(NetworkInterfacesGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveNetworkInterface[] networkInterfaces = networkInterfacesGetSync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(networkInterfaces, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // WirelessGet

    public native HveWireless wirelessGetSync(String id) throws GeException;

    public interface WirelessGetAsyncCallback {
        void callback(HveWireless wireless, GeException exception);
    }

    public void wirelessGetAsync(String id, WirelessGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveWireless wireless = wirelessGetSync(id);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(wireless, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // WirelessUpdate

    public native HveResponseStatus wirelessUpdateSync(String id, HveWireless wireless) throws GeException;

    public interface WirelessUpdateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void wirelessUpdateAsync(String id, HveWireless wireless, WirelessUpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = wirelessUpdateSync(id, wireless);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // WirelessUpdateV1

    public native boolean wirelessUpdateV1Sync(String id, HveWireless wireless) throws GeException;

    public interface WirelessUpdateV1AsyncCallback {
        void callback(GeException exception);
    }

    public void wirelessUpdateV1Async(String id, HveWireless wireless, WirelessUpdateV1AsyncCallback callback) {
        new Thread(() -> {
            try {
                wirelessUpdateV1Sync(id, wireless);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(exception));
            }
        }).start();
    }

    // IPAddressGet

    public native HveIPAddress ipAddressGetSync(String id) throws GeException;

    public interface IPAddressGetAsyncCallback {
        void callback(HveIPAddress ipAddress, GeException exception);
    }

    public void ipAddressGetAsync(String id, IPAddressGetAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveIPAddress ipAddress = ipAddressGetSync(id);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(ipAddress, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // IPAddressUpdate

    public native HveResponseStatus ipAddressUpdateSync(String id, HveIPAddress ipAddress) throws GeException;

    public interface IPAddressUpdateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void ipAddressUpdateAsync(String id, HveIPAddress ipAddress, IPAddressUpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = ipAddressUpdateSync(id, ipAddress);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // StreamingChannelUpdate

    public native HveResponseStatus streamingChannelUpdateSync(String id, HveStreamingChannel streamingChannel) throws GeException;

    public interface StreamingChannelUpdateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void streamingChannelUpdateAsync(String id, HveStreamingChannel streamingChannel, StreamingChannelUpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = streamingChannelUpdateSync(id, streamingChannel);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // MotionDetectionUpdate

    public native HveResponseStatus motionDetectionUpdateSync(String id, HveMotionDetection motionDetection) throws GeException;

    public interface MotionDetectionUpdateAsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void motionDetectionUpdateAsync(String id, HveMotionDetection motionDetection, MotionDetectionUpdateAsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = motionDetectionUpdateSync(id, motionDetection);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }
    
    // ActivateV1

    public native HveResponseStatus activateV1Sync(HveActivateInfo activateInfo) throws GeException;

    public interface ActivateV1AsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void activateV1Async(HveActivateInfo activateInfo, ActivateV1AsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = activateV1Sync(activateInfo);
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }

    // TimeUpdateV1

    public native HveResponseStatus timeUpdateV1Sync() throws GeException;

    public interface TimeUpdateV1AsyncCallback {
        void callback(HveResponseStatus responseStatus, GeException exception);
    }

    public void timeUpdateV1Async(TimeUpdateV1AsyncCallback callback) {
        new Thread(() -> {
            try {
                HveResponseStatus responseStatus = timeUpdateV1Sync();
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(responseStatus, null));
            } catch (GeException exception) {
                if (callback == null) return;
                new Handler(Looper.getMainLooper()).post(() -> callback.callback(null, exception));
            }
        }).start();
    }
}
