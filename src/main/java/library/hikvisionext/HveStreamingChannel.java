package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveStreamingChannel extends LjlObject {
    public HveVideo video;
    public HveAudio audio;
}
