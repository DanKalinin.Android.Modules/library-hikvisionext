package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveMotionDetection extends LjlObject {
    public boolean enabled;
    public HveGrid grid;
    public HveMotionDetectionLayout motionDetectionLayout;
}
