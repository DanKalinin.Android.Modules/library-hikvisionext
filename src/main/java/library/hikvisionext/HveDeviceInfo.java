package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveDeviceInfo extends LjlObject {
    public String deviceName;
    public String deviceId;
    public String deviceDescription;
    public String deviceLocation;
    public String systemContact;
    public String model;
    public String serialNumber;
    public String macAddress;
    public String firmwareVersion;
    public String firmwareReleasedDate;
    public String bootVersion;
    public String bootReleasedDate;
    public String hardwareVersion;
    public String encoderVersion;
    public String encoderReleasedDate;
    public String decoderVersion;
    public String decoderReleasedDate;
    public String deviceType;
    public int telecontrolId;
    public boolean supportBeep;
    public String firmwareVersionInfo;
}
