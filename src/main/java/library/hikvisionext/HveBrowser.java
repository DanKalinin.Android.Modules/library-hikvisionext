package library.hikvisionext;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import library.java.lang.LjlObject;

public class HveBrowser extends LjlObject implements NsdManager.DiscoveryListener {
    public interface Listener {
        void endpointFound(HveBrowser sender, HveEndpoint endpoint);
        void endpointRemoved(HveBrowser sender, String name);
    }

    public static class Listeners extends ArrayList<Listener> implements Listener {
        @Override
        public void endpointFound(HveBrowser sender, HveEndpoint endpoint) {
            for (Listener listener : this) {
                listener.endpointFound(sender, endpoint);
            }
        }

        @Override
        public void endpointRemoved(HveBrowser sender, String name) {
            for (Listener listener : this) {
                listener.endpointRemoved(sender, name);
            }
        }
    }

    public String type;
    public Listeners listeners;
    private NsdManager nsdManager;
    private boolean started;

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {

    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {

    }

    @Override
    public void onDiscoveryStarted(String serviceType) {

    }

    @Override
    public void onDiscoveryStopped(String serviceType) {

    }

    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {
        HveResolver resolver = HveResolver.resolver(this);
        nsdManager.resolveService(serviceInfo, resolver);
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        new Handler(Looper.getMainLooper()).post(() -> listeners.endpointRemoved(this, serviceInfo.getServiceName()));
    }

    public static HveBrowser browser(Context context, String type) {
        HveBrowser ret = new HveBrowser();
        ret.type = type;
        ret.listeners = new Listeners();
        ret.nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        ret.started = false;
        return ret;
    }

    public void start() {
        if (started) return;
        nsdManager.discoverServices(type, NsdManager.PROTOCOL_DNS_SD, this);
        started = true;
    }

    public void stop() {
        if (!started) return;
        nsdManager.stopServiceDiscovery(this);
        started = false;
    }
}
