package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveIPAddress extends LjlObject {
    public String ipVersion;
    public String addressingType;
    public HveIPv6Mode ipv6Mode;
}
