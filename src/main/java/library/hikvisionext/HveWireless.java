package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveWireless extends LjlObject {
    public boolean enabled;
    public String ssid;
    public HveWirelessSecurity wirelessSecurity;
}
