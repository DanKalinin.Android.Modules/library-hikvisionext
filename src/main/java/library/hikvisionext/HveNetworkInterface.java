package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveNetworkInterface extends LjlObject {
    public String id;
    public boolean defaultConnection;
    public String macAddress;
}
