package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveTime extends LjlObject {
    public String timeMode;
    public String localTime;
    public String timeZone;
}
