package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveResponseStatus extends LjlObject {
    public String requestUrl;
    public int statusCode;
    public String statusString;
    public String subStatusCode;
}
