package library.hikvisionext;

import library.java.lang.LjlObject;

public class HveEndpoint extends LjlObject {
    public String name;
    public String host;
    public String ip;
    public int port;
    public String path;
}
