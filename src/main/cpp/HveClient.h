//
// Created by Dan on 17.10.2021.
//

#ifndef LIBRARY_HIKVISIONEXT_HVECLIENT_H
#define LIBRARY_HIKVISIONEXT_HVECLIENT_H

#include "HveMain.h"
#include "HveSchema.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID object;
    jfieldID base;
    jfieldID username;
    jfieldID password;
    jmethodID init;
} HveClientType;

extern HveClientType HveClient;

void HveClientLoad(JNIEnv *env);
void HveClientUnload(JNIEnv *env);

G_END_DECLS

#endif //LIBRARY_HIKVISIONEXT_HVECLIENT_H
