//
// Created by Dan on 17.10.2021.
//

#ifndef LIBRARY_HIKVISIONEXT_HVESCHEMA_H
#define LIBRARY_HIKVISIONEXT_HVESCHEMA_H

#include "HveMain.h"










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID requestUrl;
    jfieldID statusCode;
    jfieldID statusString;
    jfieldID subStatusCode;
    jmethodID init;
} HveResponseStatusType;

extern HveResponseStatusType HveResponseStatus;

void HveResponseStatusLoad(JNIEnv *env);
void HveResponseStatusUnload(JNIEnv *env);

jobject HveResponseStatusFrom(JNIEnv *env, HVEResponseStatus *sdkResponseStatus);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID activated;
    jmethodID init;
} HveActivateStatusType;

extern HveActivateStatusType HveActivateStatus;

void HveActivateStatusLoad(JNIEnv *env);
void HveActivateStatusUnload(JNIEnv *env);

jobject HveActivateStatusFrom(JNIEnv *env, HVEActivateStatus *sdkActivateStatus);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID key;
    jmethodID init;
} HvePublicKeyType;

extern HvePublicKeyType HvePublicKey;

void HvePublicKeyLoad(JNIEnv *env);
void HvePublicKeyUnload(JNIEnv *env);

jobject HvePublicKeyFrom(JNIEnv *env, HVEPublicKey *sdkPublicKey);
HVEPublicKey *HvePublicKeyTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID key;
    jmethodID init;
} HveChallengeType;

extern HveChallengeType HveChallenge;

void HveChallengeLoad(JNIEnv *env);
void HveChallengeUnload(JNIEnv *env);

jobject HveChallengeFrom(JNIEnv *env, HVEChallenge *sdkChallenge);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID password;
    jmethodID init;
} HveActivateInfoType;

extern HveActivateInfoType HveActivateInfo;

void HveActivateInfoLoad(JNIEnv *env);
void HveActivateInfoUnload(JNIEnv *env);

jobject HveActivateInfoFrom(JNIEnv *env, HVEActivateInfo *sdkActivateInfo);
HVEActivateInfo *HveActivateInfoTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID deviceName;
    jfieldID deviceId;
    jfieldID deviceDescription;
    jfieldID deviceLocation;
    jfieldID systemContact;
    jfieldID model;
    jfieldID serialNumber;
    jfieldID macAddress;
    jfieldID firmwareVersion;
    jfieldID firmwareReleasedDate;
    jfieldID bootVersion;
    jfieldID bootReleasedDate;
    jfieldID hardwareVersion;
    jfieldID encoderVersion;
    jfieldID encoderReleasedDate;
    jfieldID decoderVersion;
    jfieldID decoderReleasedDate;
    jfieldID deviceType;
    jfieldID telecontrolId;
    jfieldID supportBeep;
    jfieldID firmwareVersionInfo;
    jmethodID init;
} HveDeviceInfoType;

extern HveDeviceInfoType HveDeviceInfo;

void HveDeviceInfoLoad(JNIEnv *env);
void HveDeviceInfoUnload(JNIEnv *env);

jobject HveDeviceInfoFrom(JNIEnv *env, HVEDeviceInfo *sdkDeviceInfo);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID timeMode;
    jfieldID localTime;
    jfieldID timeZone;
    jmethodID init;
} HveTimeType;

extern HveTimeType HveTime;

void HveTimeLoad(JNIEnv *env);
void HveTimeUnload(JNIEnv *env);

jobject HveTimeFrom(JNIEnv *env, HVETime *sdkTime);
HVETime *HveTimeTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID id;
    jfieldID defaultConnection;
    jfieldID macAddress;
    jmethodID init;
} HveNetworkInterfaceType;

extern HveNetworkInterfaceType HveNetworkInterface;

void HveNetworkInterfaceLoad(JNIEnv *env);
void HveNetworkInterfaceUnload(JNIEnv *env);

jobject HveNetworkInterfaceFrom(JNIEnv *env, HVENetworkInterface *sdkNetworkInterface);

jobjectArray HveNetworkInterfacesFrom(JNIEnv *env, GList *sdkNetworkInterfaces);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID algorithmType;
    jfieldID sharedKey;
    jfieldID wpaKeyLength;
    jmethodID init;
} HveWPAType;

extern HveWPAType HveWPA;

void HveWPALoad(JNIEnv *env);
void HveWPAUnload(JNIEnv *env);

jobject HveWPAFrom(JNIEnv *env, HVEWPA *sdkWPA);
HVEWPA *HveWPATo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID securityMode;
    jfieldID wpa;
    jmethodID init;
} HveWirelessSecurityType;

extern HveWirelessSecurityType HveWirelessSecurity;

void HveWirelessSecurityLoad(JNIEnv *env);
void HveWirelessSecurityUnload(JNIEnv *env);

jobject HveWirelessSecurityFrom(JNIEnv *env, HVEWirelessSecurity *sdkWirelessSecurity);
HVEWirelessSecurity *HveWirelessSecurityTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID enabled;
    jfieldID ssid;
    jfieldID wirelessSecurity;
    jmethodID init;
} HveWirelessType;

extern HveWirelessType HveWireless;

void HveWirelessLoad(JNIEnv *env);
void HveWirelessUnload(JNIEnv *env);

jobject HveWirelessFrom(JNIEnv *env, HVEWireless *sdkWireless);
HVEWireless *HveWirelessTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID ipv6AddressingType;
    jmethodID init;
} HveIPv6ModeType;

extern HveIPv6ModeType HveIPv6Mode;

void HveIPv6ModeLoad(JNIEnv *env);
void HveIPv6ModeUnload(JNIEnv *env);

jobject HveIPv6ModeFrom(JNIEnv *env, HVEIPv6Mode *sdkIPv6Mode);
HVEIPv6Mode *HveIPv6ModeTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID ipVersion;
    jfieldID addressingType;
    jfieldID ipv6Mode;
    jmethodID init;
} HveIPAddressType;

extern HveIPAddressType HveIPAddress;

void HveIPAddressLoad(JNIEnv *env);
void HveIPAddressUnload(JNIEnv *env);

jobject HveIPAddressFrom(JNIEnv *env, HVEIPAddress *sdkIPAddress);
HVEIPAddress *HveIPAddressTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID videoCodecType;
    jfieldID videoResolutionWidth;
    jfieldID videoResolutionHeight;
    jfieldID videoQualityControlType;
    jfieldID maxFrameRate;
    jmethodID init;
} HveVideoType;

extern HveVideoType HveVideo;

void HveVideoLoad(JNIEnv *env);
void HveVideoUnload(JNIEnv *env);

jobject HveVideoFrom(JNIEnv *env, HVEVideo *sdkVideo);
HVEVideo *HveVideoTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID enabled;
    jfieldID audioCompressionType;
    jmethodID init;
} HveAudioType;

extern HveAudioType HveAudio;

void HveAudioLoad(JNIEnv *env);
void HveAudioUnload(JNIEnv *env);

jobject HveAudioFrom(JNIEnv *env, HVEAudio *sdkAudio);
HVEAudio *HveAudioTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID video;
    jfieldID audio;
    jmethodID init;
} HveStreamingChannelType;

extern HveStreamingChannelType HveStreamingChannel;

void HveStreamingChannelLoad(JNIEnv *env);
void HveStreamingChannelUnload(JNIEnv *env);

jobject HveStreamingChannelFrom(JNIEnv *env, HVEStreamingChannel *sdkStreamingChannel);
HVEStreamingChannel *HveStreamingChannelTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID rowGranularity;
    jfieldID columnGranularity;
    jmethodID init;
} HveGridType;

extern HveGridType HveGrid;

void HveGridLoad(JNIEnv *env);
void HveGridUnload(JNIEnv *env);

jobject HveGridFrom(JNIEnv *env, HVEGrid *sdkGrid);
HVEGrid *HveGridTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID gridMap;
    jmethodID init;
} HveLayoutType;

extern HveLayoutType HveLayout;

void HveLayoutLoad(JNIEnv *env);
void HveLayoutUnload(JNIEnv *env);

jobject HveLayoutFrom(JNIEnv *env, HVELayout *sdkLayout);
HVELayout *HveLayoutTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID sensitivityLevel;
    jfieldID layout;
    jmethodID init;
} HveMotionDetectionLayoutType;

extern HveMotionDetectionLayoutType HveMotionDetectionLayout;

void HveMotionDetectionLayoutLoad(JNIEnv *env);
void HveMotionDetectionLayoutUnload(JNIEnv *env);

jobject HveMotionDetectionLayoutFrom(JNIEnv *env, HVEMotionDetectionLayout *sdkMotionDetectionLayout);
HVEMotionDetectionLayout *HveMotionDetectionLayoutTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID enabled;
    jfieldID grid;
    jfieldID motionDetectionLayout;
    jmethodID init;
} HveMotionDetectionType;

extern HveMotionDetectionType HveMotionDetection;

void HveMotionDetectionLoad(JNIEnv *env);
void HveMotionDetectionUnload(JNIEnv *env);

jobject HveMotionDetectionFrom(JNIEnv *env, HVEMotionDetection *sdkMotionDetection);
HVEMotionDetection *HveMotionDetectionTo(JNIEnv *env, jobject this);

G_END_DECLS










G_BEGIN_DECLS

typedef struct {
    jclass class;
    jfieldID name;
    jfieldID host;
    jfieldID ip;
    jfieldID port;
    jfieldID path;
    jmethodID init;
} HveEndpointType;

extern HveEndpointType HveEndpoint;

void HveEndpointLoad(JNIEnv *env);
void HveEndpointUnload(JNIEnv *env);

jobject HveEndpointFrom(JNIEnv *env, HVEEndpoint *sdkEndpoint);
HVEEndpoint *HveEndpointTo(JNIEnv *env, jobject this);

G_END_DECLS










#endif //LIBRARY_HIKVISIONEXT_HVESCHEMA_H
