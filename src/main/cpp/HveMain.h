//
// Created by Dan on 15.10.2021.
//

#ifndef LIBRARY_HIKVISIONEXT_HVEMAIN_H
#define LIBRARY_HIKVISIONEXT_HVEMAIN_H

#include <hik-vision-ext/hik-vision-ext.h>
#include <GlibExt.h>
#include <SoupExt.h>
#include <GnuTLSExt.h>
#include <GLibNetworkingExt.h>
#include <XMLExt.h>
#include <AvahiExt.h>

#endif //LIBRARY_HIKVISIONEXT_HVEMAIN_H
