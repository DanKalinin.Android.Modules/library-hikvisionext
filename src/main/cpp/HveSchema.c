//
// Created by Dan on 17.10.2021.
//

#include "HveSchema.h"










HveResponseStatusType HveResponseStatus = {0};

void HveResponseStatusLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveResponseStatus");
    HveResponseStatus.class = (*env)->NewGlobalRef(env, class);
    HveResponseStatus.requestUrl = (*env)->GetFieldID(env, class, "requestUrl", "Ljava/lang/String;");
    HveResponseStatus.statusCode = (*env)->GetFieldID(env, class, "statusCode", "I");
    HveResponseStatus.statusString = (*env)->GetFieldID(env, class, "statusString", "Ljava/lang/String;");
    HveResponseStatus.subStatusCode = (*env)->GetFieldID(env, class, "subStatusCode", "Ljava/lang/String;");
    HveResponseStatus.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveResponseStatusUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveResponseStatus.class);
}

jobject HveResponseStatusFrom(JNIEnv *env, HVEResponseStatus *sdkResponseStatus) {
    jstring requestUrl = (*env)->NewStringUTF(env, sdkResponseStatus->request_url);
    jstring statusString = (*env)->NewStringUTF(env, sdkResponseStatus->status_string);
    jstring subStatusCode = (*env)->NewStringUTF(env, sdkResponseStatus->sub_status_code);

    jobject this = (*env)->NewObject(env, HveResponseStatus.class, HveResponseStatus.init);
    (*env)->SetObjectField(env, this, HveResponseStatus.requestUrl, requestUrl);
    (*env)->SetIntField(env, this, HveResponseStatus.statusCode, sdkResponseStatus->status_code);
    (*env)->SetObjectField(env, this, HveResponseStatus.statusString, statusString);
    (*env)->SetObjectField(env, this, HveResponseStatus.subStatusCode, subStatusCode);
    return this;
}










HveActivateStatusType HveActivateStatus = {0};

void HveActivateStatusLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveActivateStatus");
    HveActivateStatus.class = (*env)->NewGlobalRef(env, class);
    HveActivateStatus.activated = (*env)->GetFieldID(env, class, "activated", "Z");
    HveActivateStatus.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveActivateStatusUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveActivateStatus.class);
}

jobject HveActivateStatusFrom(JNIEnv *env, HVEActivateStatus *sdkActivateStatus) {
    jobject this = (*env)->NewObject(env, HveActivateStatus.class, HveActivateStatus.init);
    (*env)->SetBooleanField(env, this, HveActivateStatus.activated, sdkActivateStatus->activated);
    return this;
}










HvePublicKeyType HvePublicKey = {0};

void HvePublicKeyLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HvePublicKey");
    HvePublicKey.class = (*env)->NewGlobalRef(env, class);
    HvePublicKey.key = (*env)->GetFieldID(env, class, "key", "Ljava/lang/String;");
    HvePublicKey.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HvePublicKeyUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HvePublicKey.class);
}

jobject HvePublicKeyFrom(JNIEnv *env, HVEPublicKey *sdkPublicKey) {
    jstring key = (*env)->NewStringUTF(env, sdkPublicKey->key);

    jobject this = (*env)->NewObject(env, HvePublicKey.class, HvePublicKey.init);
    (*env)->SetObjectField(env, this, HvePublicKey.key, key);
    return this;
}

HVEPublicKey *HvePublicKeyTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring key = (*env)->GetObjectField(env, this, HvePublicKey.key);

    HVEPublicKey sdkPublicKey = {0};
    sdkPublicKey.key = GeJNIEnvGetStringUTFChars(env, key, NULL);
    HVEPublicKey *ret = hve_public_key_copy(&sdkPublicKey);

    GeJNIEnvReleaseStringUTFChars(env, key, sdkPublicKey.key);

    return ret;
}










HveChallengeType HveChallenge = {0};

void HveChallengeLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveChallenge");
    HveChallenge.class = (*env)->NewGlobalRef(env, class);
    HveChallenge.key = (*env)->GetFieldID(env, class, "key", "Ljava/lang/String;");
    HveChallenge.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveChallengeUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveChallenge.class);
}

jobject HveChallengeFrom(JNIEnv *env, HVEChallenge *sdkChallenge) {
    jstring key = (*env)->NewStringUTF(env, sdkChallenge->key);

    jobject this = (*env)->NewObject(env, HveChallenge.class, HveChallenge.init);
    (*env)->SetObjectField(env, this, HveChallenge.key, key);
    return this;
}










HveActivateInfoType HveActivateInfo = {0};

void HveActivateInfoLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveActivateInfo");
    HveActivateInfo.class = (*env)->NewGlobalRef(env, class);
    HveActivateInfo.password = (*env)->GetFieldID(env, class, "password", "Ljava/lang/String;");
    HveActivateInfo.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveActivateInfoUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveActivateInfo.class);
}

jobject HveActivateInfoFrom(JNIEnv *env, HVEActivateInfo *sdkActivateInfo) {
    jstring password = (*env)->NewStringUTF(env, sdkActivateInfo->password);

    jobject this = (*env)->NewObject(env, HveActivateInfo.class, HveActivateInfo.init);
    (*env)->SetObjectField(env, this, HveActivateInfo.password, password);
    return this;
}

HVEActivateInfo *HveActivateInfoTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring password = (*env)->GetObjectField(env, this, HveActivateInfo.password);

    HVEActivateInfo sdkActivateInfo = {0};
    sdkActivateInfo.password = GeJNIEnvGetStringUTFChars(env, password, NULL);
    HVEActivateInfo *ret = hve_activate_info_copy(&sdkActivateInfo);

    GeJNIEnvReleaseStringUTFChars(env, password, sdkActivateInfo.password);

    return ret;
}










HveDeviceInfoType HveDeviceInfo = {0};

void HveDeviceInfoLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveDeviceInfo");
    HveDeviceInfo.class = (*env)->NewGlobalRef(env, class);
    HveDeviceInfo.deviceName = (*env)->GetFieldID(env, class, "deviceName", "Ljava/lang/String;");
    HveDeviceInfo.deviceId = (*env)->GetFieldID(env, class, "deviceId", "Ljava/lang/String;");
    HveDeviceInfo.deviceDescription = (*env)->GetFieldID(env, class, "deviceDescription", "Ljava/lang/String;");
    HveDeviceInfo.deviceLocation = (*env)->GetFieldID(env, class, "deviceLocation", "Ljava/lang/String;");
    HveDeviceInfo.systemContact = (*env)->GetFieldID(env, class, "systemContact", "Ljava/lang/String;");
    HveDeviceInfo.model = (*env)->GetFieldID(env, class, "model", "Ljava/lang/String;");
    HveDeviceInfo.serialNumber = (*env)->GetFieldID(env, class, "serialNumber", "Ljava/lang/String;");
    HveDeviceInfo.macAddress = (*env)->GetFieldID(env, class, "macAddress", "Ljava/lang/String;");
    HveDeviceInfo.firmwareVersion = (*env)->GetFieldID(env, class, "firmwareVersion", "Ljava/lang/String;");
    HveDeviceInfo.firmwareReleasedDate = (*env)->GetFieldID(env, class, "firmwareReleasedDate", "Ljava/lang/String;");
    HveDeviceInfo.bootVersion = (*env)->GetFieldID(env, class, "bootVersion", "Ljava/lang/String;");
    HveDeviceInfo.bootReleasedDate = (*env)->GetFieldID(env, class, "bootReleasedDate", "Ljava/lang/String;");
    HveDeviceInfo.hardwareVersion = (*env)->GetFieldID(env, class, "hardwareVersion", "Ljava/lang/String;");
    HveDeviceInfo.encoderVersion = (*env)->GetFieldID(env, class, "encoderVersion", "Ljava/lang/String;");
    HveDeviceInfo.encoderReleasedDate = (*env)->GetFieldID(env, class, "encoderReleasedDate", "Ljava/lang/String;");
    HveDeviceInfo.decoderVersion = (*env)->GetFieldID(env, class, "decoderVersion", "Ljava/lang/String;");
    HveDeviceInfo.decoderReleasedDate = (*env)->GetFieldID(env, class, "decoderReleasedDate", "Ljava/lang/String;");
    HveDeviceInfo.deviceType = (*env)->GetFieldID(env, class, "deviceType", "Ljava/lang/String;");
    HveDeviceInfo.telecontrolId = (*env)->GetFieldID(env, class, "telecontrolId", "I");
    HveDeviceInfo.supportBeep = (*env)->GetFieldID(env, class, "supportBeep", "Z");
    HveDeviceInfo.firmwareVersionInfo = (*env)->GetFieldID(env, class, "firmwareVersionInfo", "Ljava/lang/String;");
    HveDeviceInfo.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveDeviceInfoUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveDeviceInfo.class);
}

jobject HveDeviceInfoFrom(JNIEnv *env, HVEDeviceInfo *sdkDeviceInfo) {
    jstring deviceName = (*env)->NewStringUTF(env, sdkDeviceInfo->device_name);
    jstring deviceId = (*env)->NewStringUTF(env, sdkDeviceInfo->device_id);
    jstring deviceDescription = (*env)->NewStringUTF(env, sdkDeviceInfo->device_description);
    jstring deviceLocation = (*env)->NewStringUTF(env, sdkDeviceInfo->device_location);
    jstring systemContact = (*env)->NewStringUTF(env, sdkDeviceInfo->system_contact);
    jstring model = (*env)->NewStringUTF(env, sdkDeviceInfo->model);
    jstring serialNumber = (*env)->NewStringUTF(env, sdkDeviceInfo->serial_number);
    jstring macAddress = (*env)->NewStringUTF(env, sdkDeviceInfo->mac_address);
    jstring firmwareVersion = (*env)->NewStringUTF(env, sdkDeviceInfo->firmware_version);
    jstring firmwareReleasedDate = (*env)->NewStringUTF(env, sdkDeviceInfo->firmware_released_date);
    jstring bootVersion = (*env)->NewStringUTF(env, sdkDeviceInfo->boot_version);
    jstring bootReleasedDate = (*env)->NewStringUTF(env, sdkDeviceInfo->boot_released_date);
    jstring hardwareVersion = (*env)->NewStringUTF(env, sdkDeviceInfo->hardware_version);
    jstring encoderVersion = (*env)->NewStringUTF(env, sdkDeviceInfo->encoder_version);
    jstring encoderReleasedDate = (*env)->NewStringUTF(env, sdkDeviceInfo->encoder_released_date);
    jstring decoderVersion = (*env)->NewStringUTF(env, sdkDeviceInfo->decoder_version);
    jstring decoderReleasedDate = (*env)->NewStringUTF(env, sdkDeviceInfo->decoder_released_date);
    jstring deviceType = (*env)->NewStringUTF(env, sdkDeviceInfo->device_type);
    jstring firmwareVersionInfo = (*env)->NewStringUTF(env, sdkDeviceInfo->firmware_version_info);

    jobject this = (*env)->NewObject(env, HveDeviceInfo.class, HveDeviceInfo.init);
    (*env)->SetObjectField(env, this, HveDeviceInfo.deviceName, deviceName);
    (*env)->SetObjectField(env, this, HveDeviceInfo.deviceId, deviceId);
    (*env)->SetObjectField(env, this, HveDeviceInfo.deviceDescription, deviceDescription);
    (*env)->SetObjectField(env, this, HveDeviceInfo.deviceLocation, deviceLocation);
    (*env)->SetObjectField(env, this, HveDeviceInfo.systemContact, systemContact);
    (*env)->SetObjectField(env, this, HveDeviceInfo.model, model);
    (*env)->SetObjectField(env, this, HveDeviceInfo.serialNumber, serialNumber);
    (*env)->SetObjectField(env, this, HveDeviceInfo.macAddress, macAddress);
    (*env)->SetObjectField(env, this, HveDeviceInfo.firmwareVersion, firmwareVersion);
    (*env)->SetObjectField(env, this, HveDeviceInfo.firmwareReleasedDate, firmwareReleasedDate);
    (*env)->SetObjectField(env, this, HveDeviceInfo.bootVersion, bootVersion);
    (*env)->SetObjectField(env, this, HveDeviceInfo.bootReleasedDate, bootReleasedDate);
    (*env)->SetObjectField(env, this, HveDeviceInfo.hardwareVersion, hardwareVersion);
    (*env)->SetObjectField(env, this, HveDeviceInfo.encoderVersion, encoderVersion);
    (*env)->SetObjectField(env, this, HveDeviceInfo.encoderReleasedDate, encoderReleasedDate);
    (*env)->SetObjectField(env, this, HveDeviceInfo.decoderVersion, decoderVersion);
    (*env)->SetObjectField(env, this, HveDeviceInfo.decoderReleasedDate, decoderReleasedDate);
    (*env)->SetObjectField(env, this, HveDeviceInfo.deviceType, deviceType);
    (*env)->SetIntField(env, this, HveDeviceInfo.telecontrolId, sdkDeviceInfo->telecontrol_id);
    (*env)->SetBooleanField(env, this, HveDeviceInfo.supportBeep, sdkDeviceInfo->support_beep);
    (*env)->SetObjectField(env, this, HveDeviceInfo.firmwareVersionInfo, firmwareVersionInfo);
    return this;
}










HveTimeType HveTime = {0};

void HveTimeLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveTime");
    HveTime.class = (*env)->NewGlobalRef(env, class);
    HveTime.timeMode = (*env)->GetFieldID(env, class, "timeMode", "Ljava/lang/String;");
    HveTime.localTime = (*env)->GetFieldID(env, class, "localTime", "Ljava/lang/String;");
    HveTime.timeZone = (*env)->GetFieldID(env, class, "timeZone", "Ljava/lang/String;");
    HveTime.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveTimeUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveTime.class);
}

jobject HveTimeFrom(JNIEnv *env, HVETime *sdkTime) {
    jstring timeMode = (*env)->NewStringUTF(env, sdkTime->time_mode);
    jstring localTime = (*env)->NewStringUTF(env, sdkTime->local_time);
    jstring timeZone = (*env)->NewStringUTF(env, sdkTime->time_zone);

    jobject this = (*env)->NewObject(env, HveTime.class, HveTime.init);
    (*env)->SetObjectField(env, this, HveTime.timeMode, timeMode);
    (*env)->SetObjectField(env, this, HveTime.localTime, localTime);
    (*env)->SetObjectField(env, this, HveTime.timeZone, timeZone);
    return this;
}

HVETime *HveTimeTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring timeMode = (*env)->GetObjectField(env, this, HveTime.timeMode);
    jstring localTime = (*env)->GetObjectField(env, this, HveTime.localTime);
    jstring timeZone = (*env)->GetObjectField(env, this, HveTime.timeZone);

    HVETime sdkTime = {0};
    sdkTime.time_mode = GeJNIEnvGetStringUTFChars(env, timeMode, NULL);
    sdkTime.local_time = GeJNIEnvGetStringUTFChars(env, localTime, NULL);
    sdkTime.time_zone = GeJNIEnvGetStringUTFChars(env, timeZone, NULL);
    HVETime *ret = hve_time_copy(&sdkTime);

    GeJNIEnvReleaseStringUTFChars(env, timeMode, sdkTime.time_mode);
    GeJNIEnvReleaseStringUTFChars(env, localTime, sdkTime.local_time);
    GeJNIEnvReleaseStringUTFChars(env, timeZone, sdkTime.time_zone);

    return ret;
}










HveNetworkInterfaceType HveNetworkInterface = {0};

void HveNetworkInterfaceLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveNetworkInterface");
    HveNetworkInterface.class = (*env)->NewGlobalRef(env, class);
    HveNetworkInterface.id = (*env)->GetFieldID(env, class, "id", "Ljava/lang/String;");
    HveNetworkInterface.defaultConnection = (*env)->GetFieldID(env, class, "defaultConnection", "Z");
    HveNetworkInterface.macAddress = (*env)->GetFieldID(env, class, "macAddress", "Ljava/lang/String;");
    HveNetworkInterface.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveNetworkInterfaceUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveNetworkInterface.class);
}

jobject HveNetworkInterfaceFrom(JNIEnv *env, HVENetworkInterface *sdkNetworkInterface) {
    jstring id = (*env)->NewStringUTF(env, sdkNetworkInterface->id);
    jstring macAddress = (*env)->NewStringUTF(env, sdkNetworkInterface->mac_address);

    jobject this = (*env)->NewObject(env, HveNetworkInterface.class, HveNetworkInterface.init);
    (*env)->SetObjectField(env, this, HveNetworkInterface.id, id);
    (*env)->SetBooleanField(env, this, HveNetworkInterface.defaultConnection, sdkNetworkInterface->default_connection);
    (*env)->SetObjectField(env, this, HveNetworkInterface.macAddress, macAddress);
    return this;
}

jobjectArray HveNetworkInterfacesFrom(JNIEnv *env, GList *sdkNetworkInterfaces) {
    guint n = g_list_length(sdkNetworkInterfaces);
    jobjectArray this = (*env)->NewObjectArray(env, n, HveNetworkInterface.class, NULL);
    jsize i = 0;

    for (GList *sdkNetworkInterface = sdkNetworkInterfaces; sdkNetworkInterface != NULL; sdkNetworkInterface = sdkNetworkInterface->next) {
        jobject networkInterface = HveNetworkInterfaceFrom(env, sdkNetworkInterface->data);
        (*env)->SetObjectArrayElement(env, this, i, networkInterface);
        i++;
    }

    return this;
}










HveWPAType HveWPA = {0};

void HveWPALoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveWPA");
    HveWPA.class = (*env)->NewGlobalRef(env, class);
    HveWPA.algorithmType = (*env)->GetFieldID(env, class, "algorithmType", "Ljava/lang/String;");
    HveWPA.sharedKey = (*env)->GetFieldID(env, class, "sharedKey", "Ljava/lang/String;");
    HveWPA.wpaKeyLength = (*env)->GetFieldID(env, class, "wpaKeyLength", "I");
    HveWPA.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveWPAUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveWPA.class);
}

jobject HveWPAFrom(JNIEnv *env, HVEWPA *sdkWPA) {
    jstring algorithmType = (*env)->NewStringUTF(env, sdkWPA->algorithm_type);
    jstring sharedKey = (*env)->NewStringUTF(env, sdkWPA->shared_key);

    jobject this = (*env)->NewObject(env, HveWPA.class, HveWPA.init);
    (*env)->SetObjectField(env, this, HveWPA.algorithmType, algorithmType);
    (*env)->SetObjectField(env, this, HveWPA.sharedKey, sharedKey);
    (*env)->SetIntField(env, this, HveWPA.wpaKeyLength, sdkWPA->wpa_key_length);
    return this;
}

HVEWPA *HveWPATo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring algorithmType = (*env)->GetObjectField(env, this, HveWPA.algorithmType);
    jstring sharedKey = (*env)->GetObjectField(env, this, HveWPA.sharedKey);

    HVEWPA sdkWPA = {0};
    sdkWPA.algorithm_type = GeJNIEnvGetStringUTFChars(env, algorithmType, NULL);
    sdkWPA.shared_key = GeJNIEnvGetStringUTFChars(env, sharedKey, NULL);
    sdkWPA.wpa_key_length = (*env)->GetIntField(env, this, HveWPA.wpaKeyLength);
    HVEWPA *ret = hve_wpa_copy(&sdkWPA);

    GeJNIEnvReleaseStringUTFChars(env, algorithmType, sdkWPA.algorithm_type);
    GeJNIEnvReleaseStringUTFChars(env, sharedKey, sdkWPA.shared_key);

    return ret;
}










HveWirelessSecurityType HveWirelessSecurity = {0};

void HveWirelessSecurityLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveWirelessSecurity");
    HveWirelessSecurity.class = (*env)->NewGlobalRef(env, class);
    HveWirelessSecurity.securityMode = (*env)->GetFieldID(env, class, "securityMode", "Ljava/lang/String;");
    HveWirelessSecurity.wpa = (*env)->GetFieldID(env, class, "wpa", "Llibrary/hikvisionext/HveWPA;");
    HveWirelessSecurity.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveWirelessSecurityUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveWirelessSecurity.class);
}

jobject HveWirelessSecurityFrom(JNIEnv *env, HVEWirelessSecurity *sdkWirelessSecurity) {
    jstring securityMode = (*env)->NewStringUTF(env, sdkWirelessSecurity->security_mode);
    jobject wpa = HveWPAFrom(env, sdkWirelessSecurity->wpa);

    jobject this = (*env)->NewObject(env, HveWirelessSecurity.class, HveWirelessSecurity.init);
    (*env)->SetObjectField(env, this, HveWirelessSecurity.securityMode, securityMode);
    (*env)->SetObjectField(env, this, HveWirelessSecurity.wpa, wpa);
    return this;
}

HVEWirelessSecurity *HveWirelessSecurityTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring securityMode = (*env)->GetObjectField(env, this, HveWirelessSecurity.securityMode);
    jobject wpa = (*env)->GetObjectField(env, this, HveWirelessSecurity.wpa);

    HVEWirelessSecurity sdkWirelessSecurity = {0};
    sdkWirelessSecurity.security_mode = GeJNIEnvGetStringUTFChars(env, securityMode, NULL);
    HVEWirelessSecurity *ret = hve_wireless_security_copy(&sdkWirelessSecurity);
    ret->wpa = HveWPATo(env, wpa);

    GeJNIEnvReleaseStringUTFChars(env, securityMode, sdkWirelessSecurity.security_mode);

    return ret;
}










HveWirelessType HveWireless = {0};

void HveWirelessLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveWireless");
    HveWireless.class = (*env)->NewGlobalRef(env, class);
    HveWireless.enabled = (*env)->GetFieldID(env, class, "enabled", "Z");
    HveWireless.ssid = (*env)->GetFieldID(env, class, "ssid", "Ljava/lang/String;");
    HveWireless.wirelessSecurity = (*env)->GetFieldID(env, class, "wirelessSecurity", "Llibrary/hikvisionext/HveWirelessSecurity;");
    HveWireless.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveWirelessUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveWireless.class);
}

jobject HveWirelessFrom(JNIEnv *env, HVEWireless *sdkWireless) {
    jstring ssid = (*env)->NewStringUTF(env, sdkWireless->ssid);
    jobject wirelessSecurity = HveWirelessSecurityFrom(env, sdkWireless->wireless_security);

    jobject this = (*env)->NewObject(env, HveWireless.class, HveWireless.init);
    (*env)->SetBooleanField(env, this, HveWireless.enabled, sdkWireless->enabled);
    (*env)->SetObjectField(env, this, HveWireless.ssid, ssid);
    (*env)->SetObjectField(env, this, HveWireless.wirelessSecurity, wirelessSecurity);
    return this;
}

HVEWireless *HveWirelessTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring ssid = (*env)->GetObjectField(env, this, HveWireless.ssid);
    jobject wirelessSecurity = (*env)->GetObjectField(env, this, HveWireless.wirelessSecurity);

    HVEWireless sdkWireless = {0};
    sdkWireless.enabled = (*env)->GetBooleanField(env, this, HveWireless.enabled);
    sdkWireless.ssid = GeJNIEnvGetStringUTFChars(env, ssid, NULL);
    HVEWireless *ret = hve_wireless_copy(&sdkWireless);
    ret->wireless_security = HveWirelessSecurityTo(env, wirelessSecurity);

    GeJNIEnvReleaseStringUTFChars(env, ssid, sdkWireless.ssid);

    return ret;
}










HveIPv6ModeType HveIPv6Mode = {0};

void HveIPv6ModeLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveIPv6Mode");
    HveIPv6Mode.class = (*env)->NewGlobalRef(env, class);
    HveIPv6Mode.ipv6AddressingType = (*env)->GetFieldID(env, class, "ipv6AddressingType", "Ljava/lang/String;");
    HveIPv6Mode.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveIPv6ModeUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveIPv6Mode.class);
}

jobject HveIPv6ModeFrom(JNIEnv *env, HVEIPv6Mode *sdkIPv6Mode) {
    jstring ipv6AddressingType = (*env)->NewStringUTF(env, sdkIPv6Mode->ipv6_addressing_type);

    jobject this = (*env)->NewObject(env, HveIPv6Mode.class, HveIPv6Mode.init);
    (*env)->SetObjectField(env, this, HveIPv6Mode.ipv6AddressingType, ipv6AddressingType);
    return this;
}

HVEIPv6Mode *HveIPv6ModeTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring ipv6AddressingType = (*env)->GetObjectField(env, this, HveIPv6Mode.ipv6AddressingType);

    HVEIPv6Mode sdkIPv6Mode = {0};
    sdkIPv6Mode.ipv6_addressing_type = GeJNIEnvGetStringUTFChars(env, ipv6AddressingType, NULL);
    HVEIPv6Mode *ret = hve_ipv6_mode_copy(&sdkIPv6Mode);

    GeJNIEnvReleaseStringUTFChars(env, ipv6AddressingType, sdkIPv6Mode.ipv6_addressing_type);

    return ret;
}










HveIPAddressType HveIPAddress = {0};

void HveIPAddressLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveIPAddress");
    HveIPAddress.class = (*env)->NewGlobalRef(env, class);
    HveIPAddress.ipVersion = (*env)->GetFieldID(env, class, "ipVersion", "Ljava/lang/String;");
    HveIPAddress.addressingType = (*env)->GetFieldID(env, class, "addressingType", "Ljava/lang/String;");
    HveIPAddress.ipv6Mode = (*env)->GetFieldID(env, class, "ipv6Mode", "Llibrary/hikvisionext/HveIPv6Mode;");
    HveIPAddress.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveIPAddressUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveIPAddress.class);
}

jobject HveIPAddressFrom(JNIEnv *env, HVEIPAddress *sdkIPAddress) {
    jstring ipVersion = (*env)->NewStringUTF(env, sdkIPAddress->ip_version);
    jstring addressingType = (*env)->NewStringUTF(env, sdkIPAddress->addressing_type);
    jobject ipv6Mode = HveIPv6ModeFrom(env, sdkIPAddress->ipv6_mode);

    jobject this = (*env)->NewObject(env, HveIPAddress.class, HveIPAddress.init);
    (*env)->SetObjectField(env, this, HveIPAddress.ipVersion, ipVersion);
    (*env)->SetObjectField(env, this, HveIPAddress.addressingType, addressingType);
    (*env)->SetObjectField(env, this, HveIPAddress.ipv6Mode, ipv6Mode);
    return this;
}

HVEIPAddress *HveIPAddressTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring ipVersion = (*env)->GetObjectField(env, this, HveIPAddress.ipVersion);
    jstring addressingType = (*env)->GetObjectField(env, this, HveIPAddress.addressingType);
    jobject ipv6Mode = (*env)->GetObjectField(env, this, HveIPAddress.ipv6Mode);

    HVEIPAddress sdkIPAddress = {0};
    sdkIPAddress.ip_version = GeJNIEnvGetStringUTFChars(env, ipVersion, NULL);
    sdkIPAddress.addressing_type = GeJNIEnvGetStringUTFChars(env, addressingType, NULL);
    HVEIPAddress *ret = hve_ip_address_copy(&sdkIPAddress);
    ret->ipv6_mode = HveIPv6ModeTo(env, ipv6Mode);

    GeJNIEnvReleaseStringUTFChars(env, ipVersion, sdkIPAddress.ip_version);
    GeJNIEnvReleaseStringUTFChars(env, addressingType, sdkIPAddress.addressing_type);

    return ret;
}










HveVideoType HveVideo = {0};

void HveVideoLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveVideo");
    HveVideo.class = (*env)->NewGlobalRef(env, class);
    HveVideo.videoCodecType = (*env)->GetFieldID(env, class, "videoCodecType", "Ljava/lang/String;");
    HveVideo.videoResolutionWidth = (*env)->GetFieldID(env, class, "videoResolutionWidth", "I");
    HveVideo.videoResolutionHeight = (*env)->GetFieldID(env, class, "videoResolutionHeight", "I");
    HveVideo.videoQualityControlType = (*env)->GetFieldID(env, class, "videoQualityControlType", "Ljava/lang/String;");
    HveVideo.maxFrameRate = (*env)->GetFieldID(env, class, "maxFrameRate", "I");
    HveVideo.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveVideoUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveVideo.class);
}

jobject HveVideoFrom(JNIEnv *env, HVEVideo *sdkVideo) {
    jstring videoCodecType = (*env)->NewStringUTF(env, sdkVideo->video_codec_type);
    jstring videoQualityControlType = (*env)->NewStringUTF(env, sdkVideo->video_quality_control_type);

    jobject this = (*env)->NewObject(env, HveVideo.class, HveVideo.init);
    (*env)->SetObjectField(env, this, HveVideo.videoCodecType, videoCodecType);
    (*env)->SetIntField(env, this, HveVideo.videoResolutionWidth, sdkVideo->video_resolution_width);
    (*env)->SetIntField(env, this, HveVideo.videoResolutionHeight, sdkVideo->video_resolution_height);
    (*env)->SetObjectField(env, this, HveVideo.videoQualityControlType, videoQualityControlType);
    (*env)->SetIntField(env, this, HveVideo.maxFrameRate, sdkVideo->max_frame_rate);
    return this;
}

HVEVideo *HveVideoTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring videoCodecType = (*env)->GetObjectField(env, this, HveVideo.videoCodecType);
    jstring videoQualityControlType = (*env)->GetObjectField(env, this, HveVideo.videoQualityControlType);

    HVEVideo sdkVideo = {0};
    sdkVideo.video_codec_type = GeJNIEnvGetStringUTFChars(env, videoCodecType, NULL);
    sdkVideo.video_resolution_width = (*env)->GetIntField(env, this, HveVideo.videoResolutionWidth);
    sdkVideo.video_resolution_height = (*env)->GetIntField(env, this, HveVideo.videoResolutionHeight);
    sdkVideo.video_quality_control_type = GeJNIEnvGetStringUTFChars(env, videoQualityControlType, NULL);
    sdkVideo.max_frame_rate = (*env)->GetIntField(env, this, HveVideo.maxFrameRate);
    HVEVideo *ret = hve_video_copy(&sdkVideo);

    GeJNIEnvReleaseStringUTFChars(env, videoCodecType, sdkVideo.video_codec_type);
    GeJNIEnvReleaseStringUTFChars(env, videoQualityControlType, sdkVideo.video_quality_control_type);

    return ret;
}










HveAudioType HveAudio = {0};

void HveAudioLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveAudio");
    HveAudio.class = (*env)->NewGlobalRef(env, class);
    HveAudio.enabled = (*env)->GetFieldID(env, class, "enabled", "Z");
    HveAudio.audioCompressionType = (*env)->GetFieldID(env, class, "audioCompressionType", "Ljava/lang/String;");
    HveAudio.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveAudioUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveAudio.class);
}

jobject HveAudioFrom(JNIEnv *env, HVEAudio *sdkAudio) {
    jstring audioCompressionType = (*env)->NewStringUTF(env, sdkAudio->audio_compression_type);

    jobject this = (*env)->NewObject(env, HveAudio.class, HveAudio.init);
    (*env)->SetBooleanField(env, this, HveAudio.enabled, sdkAudio->enabled);
    (*env)->SetObjectField(env, this, HveAudio.audioCompressionType, audioCompressionType);
    return this;
}

HVEAudio *HveAudioTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring audioCompressionType = (*env)->GetObjectField(env, this, HveAudio.audioCompressionType);

    HVEAudio sdkAudio = {0};
    sdkAudio.enabled = (*env)->GetBooleanField(env, this, HveAudio.enabled);
    sdkAudio.audio_compression_type = GeJNIEnvGetStringUTFChars(env, audioCompressionType, NULL);
    HVEAudio *ret = hve_audio_copy(&sdkAudio);

    GeJNIEnvReleaseStringUTFChars(env, audioCompressionType, sdkAudio.audio_compression_type);

    return ret;
}










HveStreamingChannelType HveStreamingChannel = {0};

void HveStreamingChannelLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveStreamingChannel");
    HveStreamingChannel.class = (*env)->NewGlobalRef(env, class);
    HveStreamingChannel.video = (*env)->GetFieldID(env, class, "video", "Llibrary/hikvisionext/HveVideo;");
    HveStreamingChannel.audio = (*env)->GetFieldID(env, class, "audio", "Llibrary/hikvisionext/HveAudio;");
    HveStreamingChannel.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveStreamingChannelUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveStreamingChannel.class);
}

jobject HveStreamingChannelFrom(JNIEnv *env, HVEStreamingChannel *sdkStreamingChannel) {
    jobject video = HveVideoFrom(env, sdkStreamingChannel->video);
    jobject audio = HveAudioFrom(env, sdkStreamingChannel->audio);

    jobject this = (*env)->NewObject(env, HveStreamingChannel.class, HveStreamingChannel.init);
    (*env)->SetObjectField(env, this, HveStreamingChannel.video, video);
    (*env)->SetObjectField(env, this, HveStreamingChannel.audio, audio);
    return this;
}

HVEStreamingChannel *HveStreamingChannelTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jobject video = (*env)->GetObjectField(env, this, HveStreamingChannel.video);
    jobject audio = (*env)->GetObjectField(env, this, HveStreamingChannel.audio);

    HVEStreamingChannel sdkStreamingChannel = {0};
    HVEStreamingChannel *ret = hve_streaming_channel_copy(&sdkStreamingChannel);
    ret->video = HveVideoTo(env, video);
    ret->audio = HveAudioTo(env, audio);

    return ret;
}










HveGridType HveGrid = {0};

void HveGridLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveGrid");
    HveGrid.class = (*env)->NewGlobalRef(env, class);
    HveGrid.rowGranularity = (*env)->GetFieldID(env, class, "rowGranularity", "I");
    HveGrid.columnGranularity = (*env)->GetFieldID(env, class, "columnGranularity", "I");
    HveGrid.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveGridUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveGrid.class);
}

jobject HveGridFrom(JNIEnv *env, HVEGrid *sdkGrid) {
    jobject this = (*env)->NewObject(env, HveGrid.class, HveGrid.init);
    (*env)->SetIntField(env, this, HveGrid.rowGranularity, sdkGrid->row_granularity);
    (*env)->SetIntField(env, this, HveGrid.columnGranularity, sdkGrid->column_granularity);
    return this;
}

HVEGrid *HveGridTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    HVEGrid sdkGrid = {0};
    sdkGrid.row_granularity = (*env)->GetIntField(env, this, HveGrid.rowGranularity);
    sdkGrid.column_granularity = (*env)->GetIntField(env, this, HveGrid.columnGranularity);
    HVEGrid *ret = hve_grid_copy(&sdkGrid);

    return ret;
}










HveLayoutType HveLayout = {0};

void HveLayoutLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveLayout");
    HveLayout.class = (*env)->NewGlobalRef(env, class);
    HveLayout.gridMap = (*env)->GetFieldID(env, class, "gridMap", "Ljava/lang/String;");
    HveLayout.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveLayoutUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveLayout.class);
}

jobject HveLayoutFrom(JNIEnv *env, HVELayout *sdkLayout) {
    jstring gridMap = (*env)->NewStringUTF(env, sdkLayout->grid_map);

    jobject this = (*env)->NewObject(env, HveLayout.class, HveLayout.init);
    (*env)->SetObjectField(env, this, HveLayout.gridMap, gridMap);
    return this;
}

HVELayout *HveLayoutTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring gridMap = (*env)->GetObjectField(env, this, HveLayout.gridMap);

    HVELayout sdkLayout = {0};
    sdkLayout.grid_map = GeJNIEnvGetStringUTFChars(env, gridMap, NULL);
    HVELayout *ret = hve_layout_copy(&sdkLayout);

    GeJNIEnvReleaseStringUTFChars(env, gridMap, sdkLayout.grid_map);

    return ret;
}










HveMotionDetectionLayoutType HveMotionDetectionLayout = {0};

void HveMotionDetectionLayoutLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveMotionDetectionLayout");
    HveMotionDetectionLayout.class = (*env)->NewGlobalRef(env, class);
    HveMotionDetectionLayout.sensitivityLevel = (*env)->GetFieldID(env, class, "sensitivityLevel", "I");
    HveMotionDetectionLayout.layout = (*env)->GetFieldID(env, class, "layout", "Llibrary/hikvisionext/HveLayout;");
    HveMotionDetectionLayout.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveMotionDetectionLayoutUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveMotionDetectionLayout.class);
}

jobject HveMotionDetectionLayoutFrom(JNIEnv *env, HVEMotionDetectionLayout *sdkMotionDetectionLayout) {
    jobject layout = HveLayoutFrom(env, sdkMotionDetectionLayout->layout);

    jobject this = (*env)->NewObject(env, HveMotionDetectionLayout.class, HveMotionDetectionLayout.init);
    (*env)->SetIntField(env, this, HveMotionDetectionLayout.sensitivityLevel, sdkMotionDetectionLayout->sensitivity_level);
    (*env)->SetObjectField(env, this, HveMotionDetectionLayout.layout, layout);
    return this;
}

HVEMotionDetectionLayout *HveMotionDetectionLayoutTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jobject layout = (*env)->GetObjectField(env, this, HveMotionDetectionLayout.layout);

    HVEMotionDetectionLayout sdkMotionDetectionLayout = {0};
    sdkMotionDetectionLayout.sensitivity_level = (*env)->GetIntField(env, this, HveMotionDetectionLayout.sensitivityLevel);
    HVEMotionDetectionLayout *ret = hve_motion_detection_layout_copy(&sdkMotionDetectionLayout);
    ret->layout = HveLayoutTo(env, layout);

    return ret;
}










HveMotionDetectionType HveMotionDetection = {0};

void HveMotionDetectionLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveMotionDetection");
    HveMotionDetection.class = (*env)->NewGlobalRef(env, class);
    HveMotionDetection.enabled = (*env)->GetFieldID(env, class, "enabled", "Z");
    HveMotionDetection.grid = (*env)->GetFieldID(env, class, "grid", "Llibrary/hikvisionext/HveGrid;");
    HveMotionDetection.motionDetectionLayout = (*env)->GetFieldID(env, class, "motionDetectionLayout", "Llibrary/hikvisionext/HveMotionDetectionLayout;");
    HveMotionDetection.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveMotionDetectionUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveMotionDetection.class);
}

jobject HveMotionDetectionFrom(JNIEnv *env, HVEMotionDetection *sdkMotionDetection) {
    jobject grid = HveGridFrom(env, sdkMotionDetection->grid);
    jobject motionDetectionLayout = HveMotionDetectionLayoutFrom(env, sdkMotionDetection->motion_detection_layout);

    jobject this = (*env)->NewObject(env, HveMotionDetection.class, HveMotionDetection.init);
    (*env)->SetBooleanField(env, this, HveMotionDetection.enabled, sdkMotionDetection->enabled);
    (*env)->SetObjectField(env, this, HveMotionDetection.grid, grid);
    (*env)->SetObjectField(env, this, HveMotionDetection.motionDetectionLayout, motionDetectionLayout);
    return this;
}

HVEMotionDetection *HveMotionDetectionTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jobject grid = (*env)->GetObjectField(env, this, HveMotionDetection.grid);
    jobject motionDetectionLayout = (*env)->GetObjectField(env, this, HveMotionDetection.motionDetectionLayout);

    HVEMotionDetection sdkMotionDetection = {0};
    sdkMotionDetection.enabled = (*env)->GetBooleanField(env, this, HveMotionDetection.enabled);
    HVEMotionDetection *ret = hve_motion_detection_copy(&sdkMotionDetection);
    ret->grid = HveGridTo(env, grid);
    ret->motion_detection_layout = HveMotionDetectionLayoutTo(env, motionDetectionLayout);

    return ret;
}










HveEndpointType HveEndpoint = {0};

void HveEndpointLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveEndpoint");
    HveEndpoint.class = (*env)->NewGlobalRef(env, class);
    HveEndpoint.name = (*env)->GetFieldID(env, class, "name", "Ljava/lang/String;");
    HveEndpoint.host = (*env)->GetFieldID(env, class, "host", "Ljava/lang/String;");
    HveEndpoint.ip = (*env)->GetFieldID(env, class, "ip", "Ljava/lang/String;");
    HveEndpoint.port = (*env)->GetFieldID(env, class, "port", "I");
    HveEndpoint.path = (*env)->GetFieldID(env, class, "path", "Ljava/lang/String;");
    HveEndpoint.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveEndpointUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveEndpoint.class);
}

jobject HveEndpointFrom(JNIEnv *env, HVEEndpoint *sdkEndpoint) {
    jstring name = (*env)->NewStringUTF(env, sdkEndpoint->name);
    jstring host = (*env)->NewStringUTF(env, sdkEndpoint->host);
    jstring ip = (*env)->NewStringUTF(env, sdkEndpoint->ip);
    jstring path = (*env)->NewStringUTF(env, sdkEndpoint->path);

    jobject this = (*env)->NewObject(env, HveEndpoint.class, HveEndpoint.init);
    (*env)->SetObjectField(env, this, HveEndpoint.name, name);
    (*env)->SetObjectField(env, this, HveEndpoint.host, host);
    (*env)->SetObjectField(env, this, HveEndpoint.ip, ip);
    (*env)->SetIntField(env, this, HveEndpoint.port, sdkEndpoint->port);
    (*env)->SetObjectField(env, this, HveEndpoint.path, path);
    return this;
}

HVEEndpoint *HveEndpointTo(JNIEnv *env, jobject this) {
    if (this == NULL) return NULL;

    jstring name = (*env)->GetObjectField(env, this, HveEndpoint.name);
    jstring host = (*env)->GetObjectField(env, this, HveEndpoint.host);
    jstring ip = (*env)->GetObjectField(env, this, HveEndpoint.ip);
    jstring path = (*env)->GetObjectField(env, this, HveEndpoint.path);

    HVEEndpoint sdkEndpoint = {0};
    sdkEndpoint.name = GeJNIEnvGetStringUTFChars(env, name, NULL);
    sdkEndpoint.host = GeJNIEnvGetStringUTFChars(env, host, NULL);
    sdkEndpoint.ip = GeJNIEnvGetStringUTFChars(env, ip, NULL);
    sdkEndpoint.port = (*env)->GetIntField(env, this, HveEndpoint.port);
    sdkEndpoint.path = GeJNIEnvGetStringUTFChars(env, path, NULL);
    HVEEndpoint *ret = hve_endpoint_copy(&sdkEndpoint);

    GeJNIEnvReleaseStringUTFChars(env, name, sdkEndpoint.name);
    GeJNIEnvReleaseStringUTFChars(env, host, sdkEndpoint.host);
    GeJNIEnvReleaseStringUTFChars(env, ip, sdkEndpoint.ip);
    GeJNIEnvReleaseStringUTFChars(env, path, sdkEndpoint.path);

    return ret;
}
