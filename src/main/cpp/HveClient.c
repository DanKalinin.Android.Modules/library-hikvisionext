//
// Created by Dan on 17.10.2021.
//

#include "HveClient.h"

HveClientType HveClient = {0};

void HveClientLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/hikvisionext/HveClient");
    HveClient.class = (*env)->NewGlobalRef(env, class);
    HveClient.object = (*env)->GetFieldID(env, class, "object", "J");
    HveClient.base = (*env)->GetFieldID(env, class, "base", "Ljava/lang/String;");
    HveClient.username = (*env)->GetFieldID(env, class, "username", "Ljava/lang/String;");
    HveClient.password = (*env)->GetFieldID(env, class, "password", "Ljava/lang/String;");
    HveClient.init = (*env)->GetMethodID(env, class, "<init>", "()V");
}

void HveClientUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, HveClient.class);
}

JNIEXPORT void JNICALL Java_library_hikvisionext_HveClient_close(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    if (object == 0) return;
    g_object_unref(GSIZE_TO_POINTER(object));
    (*env)->SetLongField(env, this, HveClient.object, 0);
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_client(JNIEnv *env, jclass class, jstring base, jstring username, jstring password) {
    gchar *sdkBase = GeJNIEnvGetStringUTFChars(env, base, NULL);
    g_autoptr(SoupURI) sdkBase1 = soup_uri_new(sdkBase);
    GeJNIEnvReleaseStringUTFChars(env, base, sdkBase);

    gchar *sdkUsername = GeJNIEnvGetStringUTFChars(env, username, NULL);
    gchar *sdkPassword = GeJNIEnvGetStringUTFChars(env, password, NULL);
    HVESOESession *object = hve_soe_session_new(sdkBase1, sdkUsername, sdkPassword);
    GeJNIEnvReleaseStringUTFChars(env, username, sdkUsername);
    GeJNIEnvReleaseStringUTFChars(env, password, sdkPassword);

    jobject this = (*env)->NewObject(env, class, HveClient.init);
    (*env)->SetLongField(env, this, HveClient.object, GPOINTER_TO_SIZE(object));
    (*env)->SetObjectField(env, this, HveClient.base, base);
    (*env)->SetObjectField(env, this, HveClient.username, username);
    (*env)->SetObjectField(env, this, HveClient.password, password);
    return this;
}

JNIEXPORT void JNICALL Java_library_hikvisionext_HveClient_abort(JNIEnv *env, jobject this) {
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    soup_session_abort(GSIZE_TO_POINTER(object));
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_activateStatusGetSync(JNIEnv *env, jobject this) {
    jobject ret = NULL;
    g_autoptr(HVEActivateStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_activate_status_get_sync(GSIZE_TO_POINTER(object), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveActivateStatusFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_challengeGetSync(JNIEnv *env, jobject this, jobject publicKey) {
    jobject ret = NULL;
    g_autoptr(HVEChallenge) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(HVEPublicKey) sdkPublicKey = HvePublicKeyTo(env, publicKey);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_challenge_get_sync(GSIZE_TO_POINTER(object), sdkPublicKey, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveChallengeFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_activateSync(JNIEnv *env, jobject this, jobject activateInfo) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(HVEActivateInfo) sdkActivateInfo = HveActivateInfoTo(env, activateInfo);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_activate_sync(GSIZE_TO_POINTER(object), sdkActivateInfo, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_rebootSync(JNIEnv *env, jobject this) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_reboot_sync(GSIZE_TO_POINTER(object), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_deviceInfoGetSync(JNIEnv *env, jobject this) {
    jobject ret = NULL;
    g_autoptr(HVEDeviceInfo) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_device_info_get_sync(GSIZE_TO_POINTER(object), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveDeviceInfoFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_timeUpdateSync(JNIEnv *env, jobject this, jobject time) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(HVETime) sdkTime = HveTimeTo(env, time);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_time_update_sync(GSIZE_TO_POINTER(object), sdkTime, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobjectArray JNICALL Java_library_hikvisionext_HveClient_networkInterfacesGetSync(JNIEnv *env, jobject this) {
    jobjectArray ret = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autolist(HVENetworkInterface) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;

    if (hve_soe_session_network_interfaces_get_sync(GSIZE_TO_POINTER(object), &sdkRet, &sdkError)) {
        ret = HveNetworkInterfacesFrom(env, sdkRet);
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_wirelessGetSync(JNIEnv *env, jobject this, jstring id) {
    jobject ret = NULL;
    g_autoptr(HVEWireless) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_wireless_get_sync(GSIZE_TO_POINTER(object), sdkId, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveWirelessFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_wirelessUpdateSync(JNIEnv *env, jobject this, jstring id, jobject wireless) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(HVEWireless) sdkWireless = HveWirelessTo(env, wireless);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_wireless_update_sync(GSIZE_TO_POINTER(object), sdkId, sdkWireless, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jboolean JNICALL Java_library_hikvisionext_HveClient_wirelessUpdateV1Sync(JNIEnv *env, jobject this, jstring id, jobject wireless) {
    jboolean ret = JNI_FALSE;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(HVEWireless) sdkWireless = HveWirelessTo(env, wireless);
    g_autoptr(GError) sdkError = NULL;

    if (hve_soe_session_wireless_update_v1_sync(GSIZE_TO_POINTER(object), sdkId, sdkWireless, &sdkError)) {
        ret = JNI_TRUE;
    } else {
        (void)GeExceptionThrowFrom(env, sdkError);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_ipAddressGetSync(JNIEnv *env, jobject this, jstring id) {
    jobject ret = NULL;
    g_autoptr(HVEIPAddress) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_ip_address_get_sync(GSIZE_TO_POINTER(object), sdkId, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveIPAddressFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_ipAddressUpdateSync(JNIEnv *env, jobject this, jstring id, jobject ipAddress) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(HVEIPAddress) sdkIpAddress = HveIPAddressTo(env, ipAddress);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_ip_address_update_sync(GSIZE_TO_POINTER(object), sdkId, sdkIpAddress, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_streamingChannelUpdateSync(JNIEnv *env, jobject this, jstring id, jobject streamingChannel) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(HVEStreamingChannel) sdkStreamingChannel = HveStreamingChannelTo(env, streamingChannel);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_streaming_channel_update_sync(GSIZE_TO_POINTER(object), sdkId, sdkStreamingChannel, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_motionDetectionUpdateSync(JNIEnv *env, jobject this, jstring id, jobject motionDetection) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    gchar *sdkId = GeJNIEnvGetStringUTFChars(env, id, NULL);
    g_autoptr(HVEMotionDetection) sdkMotionDetection = HveMotionDetectionTo(env, motionDetection);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_motion_detection_update_sync(GSIZE_TO_POINTER(object), sdkId, sdkMotionDetection, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    GeJNIEnvReleaseStringUTFChars(env, id, sdkId);

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_activateV1Sync(JNIEnv *env, jobject this, jobject activateInfo) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(HVEActivateInfo) sdkActivateInfo = HveActivateInfoTo(env, activateInfo);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_activate_v1_sync(GSIZE_TO_POINTER(object), sdkActivateInfo, &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    return ret;
}

JNIEXPORT jobject JNICALL Java_library_hikvisionext_HveClient_timeUpdateV1Sync(JNIEnv *env, jobject this) {
    jobject ret = NULL;
    g_autoptr(HVEResponseStatus) sdkRet = NULL;
    jlong object = (*env)->GetLongField(env, this, HveClient.object);
    g_autoptr(GError) sdkError = NULL;

    if ((sdkRet = hve_soe_session_time_update_v1_sync(GSIZE_TO_POINTER(object), &sdkError)) == NULL) {
        (void)GeExceptionThrowFrom(env, sdkError);
    } else {
        ret = HveResponseStatusFrom(env, sdkRet);
    }

    return ret;
}
