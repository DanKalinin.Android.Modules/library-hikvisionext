//
// Created by Dan on 18.10.2021.
//

#include "HveInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    hve_init();

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    HveResponseStatusLoad(env);
    HveActivateStatusLoad(env);
    HvePublicKeyLoad(env);
    HveChallengeLoad(env);
    HveActivateInfoLoad(env);
    HveDeviceInfoLoad(env);
    HveTimeLoad(env);
    HveNetworkInterfaceLoad(env);
    HveWPALoad(env);
    HveWirelessSecurityLoad(env);
    HveWirelessLoad(env);
    HveIPv6ModeLoad(env);
    HveIPAddressLoad(env);
    HveVideoLoad(env);
    HveAudioLoad(env);
    HveStreamingChannelLoad(env);
    HveGridLoad(env);
    HveLayoutLoad(env);
    HveMotionDetectionLayoutLoad(env);
    HveMotionDetectionLoad(env);
    HveEndpointLoad(env);
    HveClientLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    HveResponseStatusUnload(env);
    HveActivateStatusUnload(env);
    HvePublicKeyUnload(env);
    HveChallengeUnload(env);
    HveActivateInfoUnload(env);
    HveDeviceInfoUnload(env);
    HveTimeUnload(env);
    HveNetworkInterfaceUnload(env);
    HveWPAUnload(env);
    HveWirelessSecurityUnload(env);
    HveWirelessUnload(env);
    HveIPv6ModeUnload(env);
    HveIPAddressUnload(env);
    HveVideoUnload(env);
    HveAudioUnload(env);
    HveStreamingChannelUnload(env);
    HveGridUnload(env);
    HveLayoutUnload(env);
    HveMotionDetectionLayoutUnload(env);
    HveMotionDetectionUnload(env);
    HveEndpointUnload(env);
    HveClientUnload(env);
}
